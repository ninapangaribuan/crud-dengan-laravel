<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [
        'judul',
        'ISBN',
        'pengarang',
        'tahunterbit',
        'created_at',
        'updated_at'
    ];
    public function tags(){
        return $this->hasMany('App\Tag');
    }
}
