<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\LengthAwarePaginator;

use App\Book;

class MahasiswaController extends Controller
{
    public function index()
    {   
        $books = DB::table('books')->paginate(5);

        $books = \App\Book::all();
       return view ('mahasiswa.index', ['books'=> $books]);
    }

    public function tambah()
        {
            return view ('tambah');
        }

        public function store(Request $request)
        {
            DB::table('books')->insert([
                'judul' => $request->judul,
                'ISBN' => $request->isbn,
                'pengarang'=> $request->pengarang,
                'tahunterbit' => $request->tahunterbit,
            ]);
            return redirect('/mahasiswa');
        }    

public function edit($id)
{
    $books = DB::table('books')->where('id',$id)->get();
    
	return view('edit',['books' => $books]);
 
}

public function update(Request $request)
{
	DB::table('books')->where('id',$request->id)->update([
		    'judul' => $request->judul,
            'ISBN' => $request->isbn,
            'pengarang' => $request->pengarang,
            'tahunterbit' => $request->tahunterbit
	    ]);
	return redirect('/mahasiswa');
}

    public function hapus($id)
    {
        DB::table('books')->where('id',$id)->delete();
	    return redirect('/mahasiswa');
    }

}
