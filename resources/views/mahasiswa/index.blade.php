@extends('layout.main')

@section('title', 'daftar buku')

@section('container')
<body>
<div class="container">
  <div class="row">
    <div class="col-10">
    <a href="/"> Kembali</a>
      <h1 class="mt-3">Daftar Buku</h1>
      <a href="/mahasiswa/tambah"> + Tambah Buku</a>
    
        <table class="table">
            <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">ISBN</th>
            <th scope="col">Pengarang</th>
            <th scope="col">Tahun Terbit</th>
            <th scope="col">Aksi</th>
            </tr>
            </thead>

            <tr>
            @foreach( $books as $bk)

                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$bk->judul}}</td>
                <td>{{$bk->isbn}}</td>
                <td>{{$bk->pengarang}}</td>
                <td>{{$bk->tahunterbit}}</td>
                <td>
                    <a href="/mahasiswa/edit/{{ $bk->id}}">edit</a>
                    <a onclick= "return confirm ('Apakah anda yakin ingin menghapus data ini?')"
                    href="/mahasiswa/hapus/{{ $bk->id}}">hapus</a>
                </td>
            </tr>
            @endforeach
              {{ $books->links() }}
</body>

    <style type="text/css">
		    .pagination li{
			  float: left;
			  list-style-type: none;
			  margin:5px;
      }
	  </style>

        </table>
    </div>
  </div>
</div>
@endsection
