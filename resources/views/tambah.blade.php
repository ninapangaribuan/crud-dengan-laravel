<!DOCTYPE html>
<html>
<head>
	<title>Tambah</title>
</head>
<body>
 
	<h2><a href=tambah></a></h2>
	<h3>Data Buku</h3>
	<a href="/mahasiswa"> Kembali</a>
	
	<br/>
		<br/>
 
	<form action="/mahasiswa/store" method="post">
		{{ csrf_field() }}
		Judul Buku <input type="text" name="judul" required="required"> <br/>
		ISBN <input type="text" name="isbn" required="required"> <br/>
		Pengarang<input type="text" name="pengarang" required="required"> <br/>
		Tahun Terbit<input type="number" name="tahunterbit" required="required"> <br/>

	<div class="form-group row">
        <label for="publisher" class="col-sm-2 col-form-label">Kategori</label>
       		<div class="col-sm-10">
    			<select name="publisher" class="custom_select form-control" id="publisher">
        		<option value="ABCD">I</option>
        		<option value="EFGH">II</option>
        		<option value="IJKLM">III</option>
        		</select>
    		</div>
			<input type="submit" value="Add">
	</form>
	
</body>
</html>